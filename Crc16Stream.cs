﻿using System.IO;

namespace Flac
{
	/// <summary>
	/// Represents a stream that computes a CRC checksum of the data that passes through.
	/// </summary>
	internal sealed class Crc16Stream : Stream
	{
		#region Fields

		private readonly Stream baseStream;
		private ushort crc16;

		#endregion

		#region Properties

		public Stream BaseStream
		{
			get { return this.baseStream; }
		}

		public override bool CanRead
		{
			get { return this.baseStream.CanRead; }
		}

		public override bool CanWrite
		{
			get { return this.baseStream.CanWrite; }
		}
		
		public override bool CanSeek
		{
			get { return this.baseStream.CanSeek; }
		}

		public override long Length
		{
			get { return this.baseStream.Length; }
		}

		public override long Position
		{
			get { return this.baseStream.Position; }
			set { this.baseStream.Position = value; }
		}

		public ushort Crc16
		{
			get { return this.crc16; }
			set { this.crc16 = value; }
		}

		#endregion

		#region Constructors

		public Crc16Stream(Stream baseStream)
		{
			this.baseStream = baseStream;
		}

		#endregion

		#region Methods

		public override int ReadByte()
		{
			int value = this.baseStream.ReadByte();
			if (value >= 0)
				this.crc16 = (ushort)((this.crc16 << 8) ^ Crc.Crc16Table[value ^ (this.crc16 >> 8)]);

			return value;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int n = this.baseStream.Read(buffer, offset, count);
			
			for (int i = 0; i < n; i++)
				this.crc16 = (ushort)((this.crc16 << 8) ^ Crc.Crc16Table[buffer[offset + i] ^ (this.crc16 >> 8)]);

			return n;
		}

		public override void WriteByte(byte value)
		{
			this.crc16 = (ushort)((this.crc16 << 8) ^ Crc.Crc16Table[value ^ (this.crc16 >> 8)]);

			this.baseStream.WriteByte(value);
		}
		
		public override void Write(byte[] buffer, int offset, int count)
		{
			for (int i = 0; i < count; i++)
				this.crc16 = (ushort)((this.crc16 << 8) ^ Crc.Crc16Table[buffer[offset + i] ^ (this.crc16 >> 8)]);

			this.baseStream.Write(buffer, offset, count);
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.baseStream.Seek(offset, origin);
		}

		public override void Flush()
		{
			this.baseStream.Flush();
		}

		public override void SetLength(long value)
		{
			this.baseStream.SetLength(value);
		}

		#endregion
	}
}
