﻿using System.IO;
using System.Runtime.CompilerServices;

namespace Flac
{
	/// <summary>
	/// Represents a bit decoder.
	/// </summary>
	internal sealed class BitDecoder
	{
		#region Fields

		private static readonly uint[] masks =
		{
			0x00000000u, 0x00000001u, 0x00000003u, 0x00000007u, 
			0x0000000Fu, 0x0000001Fu, 0x0000003Fu, 0x0000007Fu, 
			0x000000FFu, 0x000001FFu, 0x000003FFu, 0x000007FFu,
			0x00000FFFu, 0x00001FFFu, 0x00003FFFu, 0x00007FFFu, 
			0x0000FFFFu, 0x0001FFFFu, 0x0003FFFFu, 0x0007FFFFu,
			0x000FFFFFu, 0x001FFFFFu, 0x003FFFFFu, 0x007FFFFFu,
			0x00FFFFFFu, 0x01FFFFFFu, 0x03FFFFFFu, 0x07FFFFFFu,
			0x0FFFFFFFu, 0x1FFFFFFFu, 0x3FFFFFFFu, 0x7FFFFFFFu,
			0xFFFFFFFFu
		};

		private static readonly uint[] signMasks =
		{
			0x00000000u,
			0x00000001u, 0x00000002u, 0x00000004u, 0x00000008u,
			0x00000010u, 0x00000020u, 0x00000040u, 0x00000080u,
			0x00000100u, 0x00000200u, 0x00000400u, 0x00000800u,
			0x00001000u, 0x00002000u, 0x00004000u, 0x00008000u,
			0x00010000u, 0x00020000u, 0x00040000u, 0x00080000u,
			0x00100000u, 0x00200000u, 0x00400000u, 0x00800000u,
			0x01000000u, 0x02000000u, 0x04000000u, 0x08000000u,
			0x10000000u, 0x20000000u, 0x40000000u, 0x80000000u
		};

		private readonly Stream stream;

		private long current;
		private int bit;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new bit decoder that reads from the specified stream.
		/// </summary>
		/// <param name="stream">The base stream.</param>
		public BitDecoder(Stream stream)
		{
			this.stream = stream;
			this.Invalidate();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Invalidates the bit reader, forcing it to read a new byte on the next bit operation.
		/// </summary>
		public void Invalidate()
		{
			this.bit = -1;
		}

		/// <summary>
		/// Reads a single bit from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public uint ReadBit()
		{
			if (this.bit < 0)
			{
				this.current = this.stream.ReadByte();
				if (this.current < 0)
					throw new EndOfStreamException();

				this.bit = 7;
			}

			return (uint)(this.current >> this.bit--) & 1u;
		}

		/// <summary>
		/// Reads the specified number of bits from the stream.
		/// </summary>
		/// <param name="n">The number of bits. Must be less than 32.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public uint ReadBits(int n)
		{
#if DEBUG
			if (n <= 0 || n > 32)
				throw new System.ArgumentException("Number of bits must be between 1 and 32 (inclusive).");
#endif
			this.bit -= n;
			while (this.bit < -1)
			{
				this.current = (this.current << 8) | (byte)this.stream.ReadByte();
				this.bit += 8;
			}

			return unchecked(((uint)this.current >> (this.bit + 1)) & masks[n]);
		}

		/// <summary>
		/// Reads the specified number of bits from the stream, treating the first one as sign bit.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)] 
		public int ReadBitsSigned(int n)
		{
			uint value = this.ReadBits(n);
			if ((value & signMasks[n]) == 0)
				return (int)value;

			return unchecked((int)(value | (0 - signMasks[n])));
		}

		/// <summary>
		/// Reads a unary-encoded value (i.e. something like 0001 = 3).
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public int ReadUnary()
		{
			int value = -1;
			do
			{
				if (this.bit < 0)
				{
					this.current = this.stream.ReadByte();
					if (this.current < 0)
						throw new EndOfStreamException();

					this.bit = 7;
				}
				value++;
			} while ((this.current & (1 << this.bit--)) == 0);
			return value;
		}

		/// <summary>
		/// Reads a rice-encoded value.
		/// </summary>
		/// <param name="p">The rice parameter.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public int ReadRice(int p)
		{
			uint value;
			if (p == 0)
				value = (uint)this.ReadUnary();
			else
				value = ((uint)this.ReadUnary() << p) | this.ReadBits(p);

			return -(int)(value & 1) ^ (int)(value >> 1);
		}

		#endregion
	}
}
