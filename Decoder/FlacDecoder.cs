﻿using System;
using System.IO;
using Flac.Predictors;

namespace Flac
{
	/// <summary>
	/// Represents a FLAC stream reader/writer.
	/// </summary>
	public sealed class FlacDecoder
	{
		#region Nested Types

		private enum State
		{
			Invalid,

			StreamHeader,
			MetadataBlock,
			FrameHeader,
			SubFrameHeader,
			SubFrame,
		}

		#endregion

		#region Fields

		private readonly Crc16Stream stream;
		private readonly BitDecoder reader;
		private State state;
		private StreamInfo streamInfo;
		private FrameHeader frameHeader;
		private int currentChannel;
		
		private readonly ConstantDecoder constantDecoder;
		private readonly VerbatimDecoder verbatimDecoder;
		private readonly FixedDecoder fixedDecoder;
		private readonly LpcDecoder lpcDecoder;
		private SubFrameDecoder currentDecoder;

		private int wastedBitsPerSample;
		private readonly int[] temp;
		
		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FLAC stream that operates on the specified stream.
		/// </summary>
		public FlacDecoder(Stream stream)
		{
			this.stream = new Crc16Stream(stream);
			this.reader = new BitDecoder(this.stream);
			this.state = State.StreamHeader;

			PredictorCollection predictors = new PredictorCollection();
			this.constantDecoder = new ConstantDecoder(this.reader);
			this.verbatimDecoder = new VerbatimDecoder(this.reader);
			this.fixedDecoder = new FixedDecoder(this.reader, predictors);
			this.lpcDecoder = new LpcDecoder(this.reader, predictors);

			this.temp = new int[256];
		}

		#endregion

		#region Methods

		/// <summary>
		/// Reads the header of FLAC data embedded in an Ogg stream.
		/// </summary>
		public void ReadOggHeader()
		{
			byte[] data = stream.ReadBlocking(9);

			if (data[0] != 0x7F ||
			    data[1] != 'F' ||
			    data[2] != 'L' ||
			    data[3] != 'A' ||
			    data[4] != 'C' ||
			    data[5] != 0x01 ||
			    data[6] != 0x00)
				throw new InvalidDataException("Invalid FLAC Ogg header.");
		}

		/// <summary>
		/// Reads the stream header.
		/// </summary>
		public void ReadStreamHeader()
		{
			if (this.state != State.StreamHeader)
				throw new InvalidOperationException("Stream header already read.");

			byte[] magic = stream.ReadBlocking(4);
			
			if (magic[0] != 'f' || magic[1] != 'L' || magic[2] != 'a' || magic[3] != 'C')
				throw new InvalidDataException("Invalid magic number.");

			this.state = State.MetadataBlock;
		}

		/// <summary>
		/// Reads one metadata block.
		/// </summary>
		/// <returns></returns>
		public MetadataBlock ReadMetadataBlock()
		{
			if (this.state == State.StreamHeader)
				this.ReadStreamHeader(); 
			
			if (this.state > State.MetadataBlock)
				return null;

			int blockHeader = this.stream.ReadByte();
			if (blockHeader < 0)
				throw new EndOfStreamException();

			byte[] length = this.stream.ReadBlocking(3);
			byte[] metadata = new byte[(length[0] << 16) | (length[1] << 8) | length[2]];
			this.stream.ReadBlocking(metadata, 0, metadata.Length);

			MetadataBlock block = MetadataBlockType.CreateMetadataBlock(blockHeader & 0x7F, metadata);

			if (this.streamInfo == null)
				this.streamInfo = block as StreamInfo;

			if ((blockHeader & 0x80) != 0)
			{
				if (this.streamInfo == null)
				{
					this.state = State.Invalid;
					throw new InvalidDataException("No StreamInfo metadata block has been read.");
				}

				this.state = State.FrameHeader;
			}

			return block;
		}

		private bool frameSync(byte[] buffer)
		{
			// look for bit sequence "11111111 111110xx".
			int input;
			while ((input = this.stream.ReadByte()) > 0)
			{
				if (input == 0xFF)
				{
					input = this.stream.ReadByte();
					if (input < 0)
						return false;

					if ((input >> 1) == 0x7C)
					{
						buffer[0] = 0xFF;
						buffer[1] = (byte)input;
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// Reads a frame header.
		/// </summary>
		/// <param name="frame">The frame header.</param>
		public bool ReadFrame(out FrameHeader frame)
		{
			// read current sub frame to end.
			if (this.currentDecoder != null)
				this.skipSubFrame();

			// check state.
			if (this.state != State.FrameHeader)
				throw new InvalidOperationException(string.Format("Expected \"{0}\".", this.state));

			// search for frame sync.
			byte[] buffer = new byte[24];
			if (!this.frameSync(buffer))
			{
				frame = default(FrameHeader);
				return false;
			}

			int bufferOffset = 1;
			frame.VariableBlockSize = (buffer[bufferOffset++] & 1) != 0;

			// read the next two bytes.
			this.stream.ReadBlocking(buffer, bufferOffset, 2);
			frame.BlockSize = buffer[bufferOffset] >> 4; 
			frame.SampleRate = buffer[bufferOffset++] & 0xF;
			frame.ChannelAssignment = buffer[bufferOffset] >> 4;
			frame.BitsPerSample = (buffer[bufferOffset] >> 1) & 0x7;

			// reserved, must be 0.
			if ((buffer[bufferOffset++] & 1) != 0)
				throw new InvalidDataException("Invalid frame header.");

			// frame number, encoded in the UTF-8 variable length coding.
			frame.Number = this.readUTF8(buffer, ref bufferOffset, frame.VariableBlockSize ? 7 : 6);

			// get block size.
			if (frame.BlockSize == 0)
				throw new InvalidDataException("Invalid block size.");

			if (frame.BlockSize == 6)
			{
				this.stream.ReadBlocking(buffer, bufferOffset, 1);
				frame.BlockSize = buffer[bufferOffset] + 1;
				bufferOffset++;
			}
			else if (frame.BlockSize == 7)
			{
				this.stream.ReadBlocking(buffer, bufferOffset, 2);
				frame.BlockSize = ((buffer[bufferOffset] << 8) | buffer[bufferOffset + 1]) + 1;
				bufferOffset += 2;
			}
			else
			{
				frame.BlockSize = Constants.BlockSizes[frame.BlockSize];
			}

			// get sample rate.
			if (frame.SampleRate == 0)
			{
				if (this.streamInfo == null)
					throw new InvalidDataException("Sample rate can't be determined because no StreamInfo metadata block has been read.");

				frame.SampleRate = this.streamInfo.SampleRate;
			}
			else if (frame.SampleRate == 12)
			{
				this.stream.ReadBlocking(buffer, bufferOffset, 1);
				frame.SampleRate = buffer[bufferOffset];
				bufferOffset++;
			}
			else if (frame.SampleRate == 13 || frame.SampleRate == 14)
			{
				this.stream.ReadBlocking(buffer, bufferOffset, 2);
				frame.SampleRate = ((buffer[bufferOffset] << 8) | buffer[bufferOffset + 1]) + 1;
				bufferOffset += 2;

				if (frame.SampleRate == 14)
					frame.SampleRate *= 10;
			}
			else if (frame.SampleRate == 15)
			{
				throw new InvalidDataException("Invalid sample rate.");
			}
			else
			{
				frame.SampleRate = Constants.SampleRates[frame.SampleRate];
			}

			// check channel assignment.
			if (frame.ChannelAssignment > 10)
				throw new InvalidDataException("Invalid channel assignment.");

			// gets bits per sample.
			if (frame.BitsPerSample == 0)
			{
				if (this.streamInfo == null)
					throw new InvalidDataException("Sample rate can't be determined because no StreamInfo metadata block has been read.");

				frame.BitsPerSample = this.streamInfo.BitsPerSample;
			}
			else if (frame.BitsPerSample == 3 || frame.BitsPerSample == 7)
			{
				throw new InvalidDataException("Invalid sample size.");
			}
			else
			{
				frame.BitsPerSample = Constants.SampleSizes[frame.BitsPerSample];
			}
			
			// check CRC-8.
			int crc8 = stream.ReadByte();
			if (crc8 < 0)
				throw new EndOfStreamException();

			if (Crc.Crc8(0, buffer, 0, bufferOffset) != crc8)
				throw new InvalidDataException("Invalid CRC-8 in frame header.");

			// store frame.
			this.frameHeader = frame;

			// add CRC-8 byte to buffer in order to calculate CRC-16 of the header.
			buffer[bufferOffset++] = (byte)crc8;
			this.stream.Crc16 = Crc.Crc16(0, buffer, 0, bufferOffset);

			this.state = State.SubFrameHeader;
			this.currentChannel = 0;

			// invalidate bit reader.
			this.reader.Invalidate();

			return true;
		}

		/// <summary>
		/// Reads a sub-frame header and assigns the sample enumeration.
		/// </summary>
		public void ReadSubFrame()
		{
			// read current sub frame to end.
			if (this.currentDecoder != null)
				this.skipSubFrame();

			// check state.
			if (this.state != State.SubFrameHeader)
				throw new InvalidOperationException(string.Format("Expected \"{0}\".", this.state));

			// padding bit.
			if (this.reader.ReadBit() != 0)
				throw new InvalidDataException("Invalid sub frame header.");

			// frame type.
			int type = (int)this.reader.ReadBits(6);

			// 'wasted bits-per-sample' flag.
			if (this.reader.ReadBit() != 0)
				this.wastedBitsPerSample = this.reader.ReadUnary() + 1;
			else
				this.wastedBitsPerSample = 0;

			// difference channel has 1 bit more.
			int bitsPerSample;
			if ((this.frameHeader.ChannelAssignment == 8 && this.currentChannel == 1) ||
				(this.frameHeader.ChannelAssignment == 9 && this.currentChannel == 0) ||
				(this.frameHeader.ChannelAssignment == 10 && this.currentChannel == 1))
				bitsPerSample = this.frameHeader.BitsPerSample + 1;
			else
				bitsPerSample = this.frameHeader.BitsPerSample;

			// read the sub frame contents.
			if (type == 0)
			{
				this.currentDecoder = this.constantDecoder;
			}
			else if (type == 1)
			{
				this.currentDecoder = this.verbatimDecoder;	
			}
			else if (type >= 8 &&type <= 12)
			{
				this.fixedDecoder.Order = type & 7;
				this.currentDecoder = this.fixedDecoder;
			}
			else if (type >= 32)
			{
				this.lpcDecoder.Order = (type & 31) + 1;
				this.currentDecoder = this.lpcDecoder;
			}
			else
			{
				throw new InvalidDataException("Invalid or unsupported sub frame type.");
			}

			this.currentDecoder.BlockSize = this.frameHeader.BlockSize;
			this.currentDecoder.BitsPerSample = bitsPerSample - wastedBitsPerSample;
			this.currentDecoder.Initialize();
			
			this.state = State.SubFrame;
		}

		/// <summary>
		/// Reads the specified number of samples from the current sub frame into the buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		/// <returns></returns>
		public int ReadSamples(int[] buffer, int offset, int count)
		{
			if(this.state != State.SubFrame)
				throw new InvalidOperationException(string.Format("Expected \"{0}\".", this.state));
			
			count = this.currentDecoder.Decode(buffer, offset, count);

			if (count == 0)
			{
				// end of sub frame.
				this.currentDecoder = null;
				this.currentChannel++;
				this.state = State.SubFrameHeader;

				// did we read all sub frames of the current frame?
				if (this.currentChannel >= this.frameHeader.Channels)
				{
					// read frame footer.
					// back up current CRC-16.
					int streamCrc16 = this.stream.Crc16;

					// read CRC-16 from stream.
					byte[] footer = this.stream.ReadBlocking(2);
					int crc16 = (footer[0] << 8) | footer[1];

					if (crc16 != streamCrc16)
						throw new InvalidDataException("Invalid CRC-16 in frame footer.");

					this.state = State.FrameHeader;
				}
			}
			else if (this.wastedBitsPerSample > 0)
			{
				// shift samples.
				for (int i = 0; i < count; i++)
					buffer[offset + i] <<= this.wastedBitsPerSample;
			}

			return count;
		}

		/// <summary>
		/// Reads all samples of the current frame.
		/// </summary>
		public void ReadSamples(int[][] samples)
		{
			if (this.state == State.FrameHeader)
				this.ReadFrame(out this.frameHeader);

			if (this.currentChannel != 0 || this.state != State.SubFrameHeader)
				throw new InvalidOperationException("Some samples of the current frame have already been decoded.");

			if (samples.Length < this.frameHeader.Channels)
				throw new ArgumentException("Sample buffer can't hold samples for all channels.");

			for (int i = 0; i < samples.Length; i++)
			{
				if (samples[i].Length < this.frameHeader.BlockSize)
					throw new ArgumentException(string.Format("Sample buffer {0} is too small.", i));

				// read header...
				this.ReadSubFrame();

				// ... and contents.
				if (this.ReadSamples(samples[i], 0, this.frameHeader.BlockSize) != this.frameHeader.BlockSize)
					throw new InvalidOperationException("Some samples of the current frame have already been decoded.");
			}

			// undo inter-channel decorrelation.
			if (this.frameHeader.ChannelAssignment == 8)
				InterChannel.LeftSide(samples[0], samples[1], this.frameHeader.BlockSize);
			else if (this.frameHeader.ChannelAssignment == 9)
				InterChannel.RightSide(samples[0], samples[1], this.frameHeader.BlockSize);
			else if (this.frameHeader.ChannelAssignment == 10)
				InterChannel.MidSide(samples[0], samples[1], this.frameHeader.BlockSize);
		}

		/// <summary>
		/// Reads all samples of the current frame.
		/// </summary>
		/// <returns>An array of sample buffers for the individual channels.</returns>
		public int[][] ReadSamples()
		{
			// create buffers.
			int[][] samples = new int[this.frameHeader.Channels][];
			for (int i = 0; i < samples.Length; i++)
				samples[i] = new int[this.frameHeader.BlockSize];

			this.ReadSamples(samples);

			return samples;
		}

		private void skipSubFrame()
		{
			while (this.ReadSamples(temp, 0, temp.Length) > 0) ;
		}

		private long readUTF8(byte[] buffer, ref int offset, int length)
		{
			int b = this.stream.ReadByte();
			if (b < 0)
				throw new EndOfStreamException();

			buffer[offset++] = (byte)b;

			if (b < 0x80)
				return b;
			
			if (b >= 0xC0)
			{
				int mask = 0x20;
				int numBytes = 1;
				while (mask > 0 && (b & mask) != 0)
				{
					numBytes++;
					mask >>= 1;
				}

				if (mask == 0)
					throw new InvalidDataException("Invalid UTF-8 data.");

				if (numBytes + 1 > length)
					throw new InvalidDataException("UTF-8 number too long.");

				long value = (b & (mask - 1));
				
				for (int i = 0; i < numBytes; i++)
				{
					b = this.stream.ReadByte();
					if (b < 0)
						throw new EndOfStreamException();

					if ((b & 0xC0) != 0x80)
						throw new InvalidDataException("Invalid UTF-8 data.");

					value = (value << 6) | (b & 0x3Fu);
					buffer[offset++] = (byte)b;
				}

				return value;
			}

			throw new InvalidDataException("Invalid UTF-8 data.");
		}
		
		#endregion
	}
}
