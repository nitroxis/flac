﻿using Flac.Predictors;

namespace Flac
{
	/// <summary>
	/// Implements a fixed prediction sub frame decoder.
	/// </summary>
	internal sealed class FixedDecoder : PredictorDecoder
	{
		#region Fields

		private readonly PredictorCollection predictors;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new fixed sub frame decoder.
		/// </summary>
		///<param name="reader"></param> 
		/// <param name="predictors"></param>
		public FixedDecoder(BitDecoder reader, PredictorCollection predictors)
			: base(reader)
		{
			this.predictors = predictors;
		}

		#endregion

		#region Methods

		public override void Initialize()
		{
			// read warm-up samples.
			this.warmupSamples = new int[this.Order];
			for (int i = 0; i < this.Order; i++)
				this.warmupSamples[i] = this.reader.ReadBitsSigned(this.BitsPerSample);

			// get predictor.
			this.predictor = this.predictors.GetFixedPredictor(this.Order, warmupSamples);

			base.Initialize();
		}

		#endregion
	}
}
