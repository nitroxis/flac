﻿using System.IO;

namespace Flac
{
	/// <summary>
	/// Residual decoder.
	/// </summary>
	internal sealed class ResidualDecoder
	{
		#region Fields

		private readonly BitDecoder reader;

		private int blockSize;
		private int skipSamples;

		private int riceSize;
		private int riceMax;
		private int riceParameter; 
		private int bitsPerSample;
		private int sampleIndex;
		private int sampleCount;

		#endregion

		#region Properties

		public int BlockSize
		{
			get { return this.blockSize; }
			set { this.blockSize = value; }
		}

		/// <summary>
		/// Gets or sets the number of samples to skip at the beginning.
		/// </summary>
		public int SkipSamples
		{
			get { return this.skipSamples; }
			set { this.skipSamples = value; }
		}

		#endregion

		#region Constructor

		/// <summary>
		/// Creates a new residual decoder.
		/// </summary>
		/// <param name="reader"></param>
		public ResidualDecoder(BitDecoder reader)
		{
			this.reader = reader;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the residual decoder.
		/// </summary>
		public void Initialize()
		{
			// read coding method.
			uint method = this.reader.ReadBits(2);

			if (method == 0)
				this.riceSize = 4;
			else if (method == 1)
				this.riceSize = 5;
			else
				throw new InvalidDataException("Invalid residual coding method.");

			this.riceMax = (1 << riceSize) - 1;

			// read partition order.
			int partitionOrder = (int)this.reader.ReadBits(4);

			// number of samples in a partition.
			this.sampleCount = this.blockSize >> partitionOrder;
			if (sampleCount < this.skipSamples || sampleCount < 1 || (sampleCount << partitionOrder) != this.blockSize)
				throw new InvalidDataException("Invalid partition order.");

			// we have to skip the warm-up samples (the number of warm-up samples equals the order of the predictor).
			this.sampleIndex = this.skipSamples;

			// read header of first partition.
			this.readPartitionHeader();
		}

		private void readPartitionHeader()
		{
			// read rice parameter. 
			// if this is equal to riceMax, read the number of bits per sample.
			this.riceParameter = (int)this.reader.ReadBits(this.riceSize);
			if (this.riceParameter == this.riceMax)
				this.bitsPerSample = (int)this.reader.ReadBits(this.riceSize);
			else
				this.bitsPerSample = -1;
		}

		public void Decode(int[] buffer, int offset, int count)
		{
			for (int i = 0; i < count; i++)
			{
				// did we reach the end of the partition?
				if (this.sampleIndex >= this.sampleCount)
				{
					// read new partition header.
					this.readPartitionHeader();
					this.sampleIndex = 0;
				}

				// decode residual.
				if (this.bitsPerSample < 0)
					buffer[offset + i] = this.reader.ReadRice(this.riceParameter);
				else 
					buffer[offset + i] = this.reader.ReadBitsSigned(this.bitsPerSample);

				this.sampleIndex++;
			}
		}

		#endregion
	}
}
