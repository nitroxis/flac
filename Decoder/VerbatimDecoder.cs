﻿namespace Flac
{
	/// <summary>
	/// Implements a verbatim sub frame which stores all samples uncompressed.
	/// </summary>
	internal sealed class VerbatimDecoder : SubFrameDecoder
	{
		#region Fields

		private int sampleIndex;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new verbatim sub frame decoder.
		/// </summary>
		/// <param name="reader"></param>
		public VerbatimDecoder(BitDecoder reader)
			: base(reader)
		{
			
		}

		#endregion

		#region Methods

		public override void Initialize()
		{
			this.sampleIndex = 0;
		}

		public override int Decode(int[] buffer, int offset, int count)
		{
			if (count > this.BlockSize - this.sampleIndex)
				count = this.BlockSize - this.sampleIndex;

			this.sampleIndex += count; 

			for (int i = 0; i < count; i++)
				buffer[offset + i] = this.reader.ReadBitsSigned(this.BitsPerSample);

			return count;
		}
	
		#endregion
	}
}
