﻿using System;
using Flac.Predictors;

namespace Flac
{
	/// <summary>
	/// Implements a sub frame decoder that uses prediction and residual.
	/// </summary>
	internal abstract class PredictorDecoder : SubFrameDecoder
	{
		#region Fields

		private readonly ResidualDecoder residual;

		private int order;

		private int sampleIndex;
		private int sample0;

		protected int[] warmupSamples;
		protected Predictor predictor;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the order of the decoder.
		/// </summary>
		public int Order
		{
			get { return this.order; }
			set { this.order = value; }
		}

		#endregion

		#region Constructors

		protected PredictorDecoder(BitDecoder reader)
			: base(reader)
		{
			this.residual = new ResidualDecoder(reader);
		}

		#endregion

		#region Methods

		public override void Initialize()
		{
			this.sampleIndex = 0;

			// initialize residual decoder.
			this.residual.BlockSize = this.BlockSize;
			this.residual.SkipSamples = this.order;
			this.residual.Initialize();
		}

		public override int Decode(int[] buffer, int offset, int count)
		{
			int warmupCount = 0;

			if (count > this.BlockSize - this.sampleIndex)
				count = this.BlockSize - this.sampleIndex;

			if (this.sampleIndex < this.warmupSamples.Length)
			{
				warmupCount = Math.Min(count, this.warmupSamples.Length - this.sampleIndex);
				for (int i = 0; i < warmupCount; i++)
				{
					this.sample0 = this.warmupSamples[this.sampleIndex + i];
					buffer[offset + i] = this.sample0;
				}
			}

			this.residual.Decode(buffer, offset + warmupCount, count - warmupCount);
			for (int i = warmupCount; i < count; i++)
			{
				this.sample0 = buffer[offset + i] + this.predictor.Next(this.sample0);
				buffer[offset + i] = this.sample0;
			}

			this.sampleIndex += count;
			return count;
		}

		#endregion
	}
}
