﻿using System.IO;
using Flac.Predictors;

namespace Flac
{
	/// <summary>
	/// Implements an LPC predicition sub frame decoder.
	/// </summary>
	internal sealed class LpcDecoder : PredictorDecoder
	{
		#region Fields

		private readonly PredictorCollection predictors;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LPC sub frame decoder.
		/// </summary>
		public LpcDecoder(BitDecoder reader, PredictorCollection predictors)
			: base(reader)
		{
			this.predictors = predictors;
		}

		#endregion

		#region Methods

		public override void Initialize()
		{
			// read warm-up samples.
			this.warmupSamples = new int[this.Order];
			for (int i = 0; i < this.Order; i++)
				this.warmupSamples[i] = this.reader.ReadBitsSigned(this.BitsPerSample);

			// get coefficient precision.
			int precision = (int)this.reader.ReadBits(4) + 1;
			if (precision == 16)
				throw new InvalidDataException("Invalid coefficient precision.");

			// read shift.
			int shift = this.reader.ReadBitsSigned(5);

			// read coefficients.
			int[] coefficients = new int[this.Order];
			for (int i = 0; i < this.Order; i++)
				coefficients[i] = this.reader.ReadBitsSigned(precision);

			// get predictor.
			this.predictor = this.predictors.GetLpcPredictor(coefficients, warmupSamples, shift);

			base.Initialize();
		}

		#endregion
	}
}
