﻿namespace Flac
{
	/// <summary>
	/// Implements a constant sub frame.
	/// </summary>
	internal sealed class ConstantDecoder : SubFrameDecoder
	{
		#region Fields

		private int sampleIndex;
		private int value;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new constant sub frame decoder.
		/// </summary>
		/// <param name="reader"></param>
		public ConstantDecoder(BitDecoder reader)
			: base(reader)
		{
			
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the constant sub frame decoder.
		/// This reads the constant sample value from the reader.
		/// </summary>
		public override void Initialize()
		{
			this.sampleIndex = 0;
			
			// read the constant value from the stream.
			this.value = this.reader.ReadBitsSigned(this.BitsPerSample);
		}

		public override int Decode(int[] buffer, int offset, int count)
		{
			if (count > this.BlockSize - this.sampleIndex)
				count = this.BlockSize - this.sampleIndex;

			this.sampleIndex += count; 
			
			for (int i = 0; i < count; i++)
				buffer[offset + i] = this.value;

			return count;
		}
	
		#endregion
	}
}
