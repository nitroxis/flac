﻿namespace Flac
{
	/// <summary>
	/// Represents an abstract FLAC sub frame coding.
	/// </summary>
	internal abstract class SubFrameDecoder
	{
		#region Fields

		protected readonly BitDecoder reader;
		private int blockSize;
		private int bitsPerSample;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the block size.
		/// </summary>
		public int BlockSize
		{
			get { return this.blockSize; }
			set { this.blockSize = value; }
		}

		/// <summary>
		/// Gets or sets the number of bits per sample.
		/// </summary>
		public int BitsPerSample 
		{
			get { return this.bitsPerSample; }
			set { this.bitsPerSample = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new sub frame decoder that reads from the specified bit reader.
		/// </summary>
		/// <param name="reader">The bit reader to read from.</param>
		protected SubFrameDecoder(BitDecoder reader)
		{
			this.reader = reader;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the sub frame by reading from the bit reader.
		/// </summary>
		public abstract void Initialize();

		/// <summary>
		/// Decodes samples.
		/// </summary>
		/// <param name="buffer">The buffer to store the samples in.</param>
		/// <param name="offset">An offset in the buffer.</param>
		/// <param name="count">The maximum number of samples to decode.</param>
		/// <returns>The number of decoded samples.</returns>
		public abstract int Decode(int[] buffer, int offset, int count);

		#endregion
	}
}
