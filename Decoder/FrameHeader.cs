﻿using System.IO;

namespace Flac
{
	/// <summary>
	/// Represents a FLAC frame header.
	/// </summary>
	public struct FrameHeader
	{
		#region Fields

		/// <summary>
		/// The blocking strategy.
		/// False means fixed block size. The "Number" field encodes the frame number.
		/// True means variable block size. The "Number" field encodes the sample number.
		/// </summary>
		public bool VariableBlockSize;

		/// <summary>
		/// The block size in samples.
		/// </summary>
		public int BlockSize;

		/// <summary>
		/// The sample rate in Hz.
		/// </summary>
		public int SampleRate;

		/// <summary>
		/// The channel assignment.
		/// </summary>
		public int ChannelAssignment;

		/// <summary>
		/// The number of bits per sample.
		/// </summary>
		public int BitsPerSample;

		/// <summary>
		/// Represents either the frame number or the sample number, depending on the "VariableBlockSize" field.
		/// </summary>
		public long Number;

		#endregion

		#region Properties

		/// <summary>
		/// The number of channels (and thus sub frame count).
		/// </summary>
		public int Channels
		{
			get { return Constants.ChannelCounts[this.ChannelAssignment]; }
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			string channels;
			if (this.ChannelAssignment == 0)
				channels = "mono";
			else if (this.ChannelAssignment == 1)
				channels = "stereo";
			else if (this.ChannelAssignment < 8)
				channels = string.Format("{0} channels", ChannelAssignment + 1);
			else if (this.ChannelAssignment == 8)
				channels = "left/side stereo";
			else if (this.ChannelAssignment == 9)
				channels = "right/side stereo";
			else if (this.ChannelAssignment == 10)
				channels = "mid/side stereo";
			else
				throw new InvalidDataException("Invalid channel assignment.");

			return string.Format("#{0}: {1} {2}-bit samples @{3}Hz ({4})", this.Number, this.BlockSize, this.BitsPerSample, this.SampleRate, channels);
		}

		#endregion
	}
}
