﻿using System;

namespace Flac.Predictors
{
	/// <summary>
	/// A cache of predictors.
	/// </summary>
	internal sealed class PredictorCollection
	{
		#region Fields

		private readonly LpcPredictor[] fixedPredictors;
		private readonly LpcPredictor[] noShift;
		private readonly LpcPredictor[] leftShift;
		private readonly LpcPredictor[] rightShift;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new PredictorCache.
		/// </summary>
		public PredictorCollection()
		{
			this.fixedPredictors = new LpcPredictor[5];
			this.fixedPredictors[0] = new Lpc0Predictor();
			this.fixedPredictors[1] = new Fixed1Predictor();
			this.fixedPredictors[2] = new Fixed2Predictor();
			this.fixedPredictors[3] = new Fixed3Predictor();
			this.fixedPredictors[4] = new Fixed4Predictor();

			this.noShift = new LpcPredictor[16];
			this.noShift[0] = new Lpc1Predictor();
			this.noShift[1] = new Lpc2Predictor();
			this.noShift[2] = new Lpc3Predictor();
			this.noShift[3] = new Lpc4Predictor();
			this.noShift[4] = new Lpc5Predictor();
			this.noShift[5] = new Lpc6Predictor();
			this.noShift[6] = new Lpc7Predictor();
			this.noShift[7] = new Lpc8Predictor();
			this.noShift[8] = new Lpc9Predictor();
			this.noShift[9] = new Lpc10Predictor();
			this.noShift[10] = new Lpc11Predictor();
			this.noShift[11] = new Lpc12Predictor();
			this.noShift[12] = new Lpc13Predictor();
			this.noShift[13] = new Lpc14Predictor();
			this.noShift[14] = new Lpc15Predictor();
			this.noShift[15] = new Lpc16Predictor();

			this.leftShift = new LpcPredictor[16];
			this.leftShift[0] = new Lpc1LPredictor();
			this.leftShift[1] = new Lpc2LPredictor();
			this.leftShift[2] = new Lpc3LPredictor();
			this.leftShift[3] = new Lpc4LPredictor();
			this.leftShift[4] = new Lpc5LPredictor();
			this.leftShift[5] = new Lpc6LPredictor();
			this.leftShift[6] = new Lpc7LPredictor();
			this.leftShift[7] = new Lpc8LPredictor();
			this.leftShift[8] = new Lpc9LPredictor();
			this.leftShift[9] = new Lpc10LPredictor();
			this.leftShift[10] = new Lpc11LPredictor();
			this.leftShift[11] = new Lpc12LPredictor();
			this.leftShift[12] = new Lpc13LPredictor();
			this.leftShift[13] = new Lpc14LPredictor();
			this.leftShift[14] = new Lpc15LPredictor();
			this.leftShift[15] = new Lpc16LPredictor();

			this.rightShift = new LpcPredictor[16];
			this.rightShift[0] = new Lpc1RPredictor();
			this.rightShift[1] = new Lpc2RPredictor();
			this.rightShift[2] = new Lpc3RPredictor();
			this.rightShift[3] = new Lpc4RPredictor();
			this.rightShift[4] = new Lpc5RPredictor();
			this.rightShift[5] = new Lpc6RPredictor();
			this.rightShift[6] = new Lpc7RPredictor();
			this.rightShift[7] = new Lpc8RPredictor();
			this.rightShift[8] = new Lpc9RPredictor();
			this.rightShift[9] = new Lpc10RPredictor();
			this.rightShift[10] = new Lpc11RPredictor();
			this.rightShift[11] = new Lpc12RPredictor();
			this.rightShift[12] = new Lpc13RPredictor();
			this.rightShift[13] = new Lpc14RPredictor();
			this.rightShift[14] = new Lpc15RPredictor();
			this.rightShift[15] = new Lpc16RPredictor();
		}

		#endregion

		#region Methods

		public Predictor GetFixedPredictor(int order, int[] samples)
		{
			LpcPredictor predictor = this.fixedPredictors[order];
			predictor.SetSamples(samples);
			return predictor;
		}

		public Predictor GetLpcPredictor(int[] coefficients, int[] samples, int shift)
		{
			if (coefficients.Length == 0)
				throw new ArgumentException("Order must be greater than 0.");

			LpcPredictor[] array;
			if(shift > 0)
				array = this.rightShift;
			else if (shift < 0)
				array = this.leftShift;
			else
				array = this.noShift;

			int index = coefficients.Length - 1;
			if (index >= array.Length)
				return new LpcNPredictor(shift, coefficients, samples);

			LpcPredictor predictor = array[index];

			predictor.SetCoefficients(coefficients);
			predictor.SetSamples(samples);

			if (shift != 0)
				((LpcShiftPredictor)predictor).Shift = Math.Abs(shift);

			return predictor;
		}

		#endregion
	}
}
