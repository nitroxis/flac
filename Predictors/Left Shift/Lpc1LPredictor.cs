namespace Flac.Predictors
{
	internal sealed class Lpc1LPredictor : LpcShiftPredictor
	{
		#region Fields

		private int coefficient0;

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] coefficients)
		{
			this.coefficient0 = coefficients[0];
		}

		public override void SetSamples(params int[] samples)
		{
		}

		public override int Next(int sample0)
		{
			int value = sample0 * this.coefficient0;
			return value << this.Shift;
		}

		#endregion
	}
}
