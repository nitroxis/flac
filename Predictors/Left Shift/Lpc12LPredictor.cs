namespace Flac.Predictors
{
	internal sealed class Lpc12LPredictor : LpcShiftPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int coefficient5;
		private int coefficient6;
		private int coefficient7;
		private int coefficient8;
		private int coefficient9;
		private int coefficient10;
		private int coefficient11;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;
		private int sample5;
		private int sample6;
		private int sample7;
		private int sample8;
		private int sample9;
		private int sample10;
		private int sample11;

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
			this.coefficient5 = coefficients[5];
			this.coefficient6 = coefficients[6];
			this.coefficient7 = coefficients[7];
			this.coefficient8 = coefficients[8];
			this.coefficient9 = coefficients[9];
			this.coefficient10 = coefficients[10];
			this.coefficient11 = coefficients[11];
		}

		public override void SetSamples(params int[] samples)
		{
			this.sample11 = samples[0];
			this.sample10 = samples[1];
			this.sample9 = samples[2];
			this.sample8 = samples[3];
			this.sample7 = samples[4];
			this.sample6 = samples[5];
			this.sample5 = samples[6];
			this.sample4 = samples[7];
			this.sample3 = samples[8];
			this.sample2 = samples[9];
			this.sample1 = samples[10];
		}

		public override int Next(int sample0)
		{
			int value = sample0 * this.coefficient0 + this.sample1 * this.coefficient1 + this.sample2 * this.coefficient2 + this.sample3 * this.coefficient3 + this.sample4 * this.coefficient4 + this.sample5 * this.coefficient5 + this.sample6 * this.coefficient6 + this.sample7 * this.coefficient7 + this.sample8 * this.coefficient8 + this.sample9 * this.coefficient9 + this.sample10 * this.coefficient10 + this.sample11 * this.coefficient11;
			this.sample11 = this.sample10;
			this.sample10 = this.sample9;
			this.sample9 = this.sample8;
			this.sample8 = this.sample7;
			this.sample7 = this.sample6;
			this.sample6 = this.sample5;
			this.sample5 = this.sample4;
			this.sample4 = this.sample3;
			this.sample3 = this.sample2;
			this.sample2 = this.sample1;
			this.sample1 = sample0;
			return value << this.Shift;
		}

		#endregion
	}
}
