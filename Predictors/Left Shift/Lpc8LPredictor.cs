namespace Flac.Predictors
{
	internal sealed class Lpc8LPredictor : LpcShiftPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int coefficient5;
		private int coefficient6;
		private int coefficient7;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;
		private int sample5;
		private int sample6;
		private int sample7;

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
			this.coefficient5 = coefficients[5];
			this.coefficient6 = coefficients[6];
			this.coefficient7 = coefficients[7];
		}

		public override void SetSamples(params int[] samples)
		{
			this.sample7 = samples[0];
			this.sample6 = samples[1];
			this.sample5 = samples[2];
			this.sample4 = samples[3];
			this.sample3 = samples[4];
			this.sample2 = samples[5];
			this.sample1 = samples[6];
		}

		public override int Next(int sample0)
		{
			int value = sample0 * this.coefficient0 + this.sample1 * this.coefficient1 + this.sample2 * this.coefficient2 + this.sample3 * this.coefficient3 + this.sample4 * this.coefficient4 + this.sample5 * this.coefficient5 + this.sample6 * this.coefficient6 + this.sample7 * this.coefficient7;
			this.sample7 = this.sample6;
			this.sample6 = this.sample5;
			this.sample5 = this.sample4;
			this.sample4 = this.sample3;
			this.sample3 = this.sample2;
			this.sample2 = this.sample1;
			this.sample1 = sample0;
			return value << this.Shift;
		}

		#endregion
	}
}
