namespace Flac.Predictors
{
	internal sealed class Lpc2LPredictor : LpcShiftPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int sample1;

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
		}

		public override void SetSamples(params int[] samples)
		{
			this.sample1 = samples[0];
		}

		public override int Next(int sample0)
		{
			int value = sample0 * this.coefficient0 + this.sample1 * this.coefficient1;
			this.sample1 = sample0;
			return value << this.Shift;
		}

		#endregion
	}
}
