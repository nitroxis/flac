﻿namespace Flac.Predictors
{
	/// <summary>
	/// LPC null predictor.
	/// </summary>
	internal sealed class Lpc0Predictor : LpcPredictor
	{
		#region Methods

		public override int Next(int sample0)
		{
			return 0;
		}

		public override void SetCoefficients(params int[] coefficients)
		{

		}

		public override void SetSamples(params int[] samples)
		{

		}

		#endregion
	}
}
