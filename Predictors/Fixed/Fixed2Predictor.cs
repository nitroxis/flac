using System;

namespace Flac.Predictors
{
	internal sealed class Fixed2Predictor : LpcPredictor
	{
		#region Fields

		private int sample1;

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] coefficients)
		{
			throw new InvalidOperationException("Coefficients of fixed predictors can't be changed.");
		}

		public override void SetSamples(params int[] samples)
		{
			this.sample1 = samples[0];
		}

		public override int Next(int sample0)
		{
			int value = sample0 * 2 - this.sample1;
			this.sample1 = sample0;
			return value;
		}

		#endregion
	}
}
