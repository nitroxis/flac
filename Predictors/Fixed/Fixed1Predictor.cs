using System;

namespace Flac.Predictors
{
	internal sealed class Fixed1Predictor : LpcPredictor
	{
		#region Fields

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] coefficients)
		{
			throw new InvalidOperationException("Coefficients of fixed predictors can't be changed.");
		}

		public override void SetSamples(params int[] samples)
		{
		}

		public override int Next(int sample0)
		{
			return sample0;
		}

		#endregion
	}
}
