using System;

namespace Flac.Predictors
{
	internal sealed class Fixed3Predictor : LpcPredictor
	{
		#region Fields

		private int sample1;
		private int sample2;

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] coefficients)
		{
			throw new InvalidOperationException("Coefficients of fixed predictors can't be changed.");
		}

		public override void SetSamples(params int[] samples)
		{
			this.sample2 = samples[0];
			this.sample1 = samples[1];
		}

		public override int Next(int sample0)
		{
			int value = sample0 * 3 - this.sample1 * 3 + this.sample2;
			this.sample2 = this.sample1;
			this.sample1 = sample0;
			return value;
		}

		#endregion
	}
}
