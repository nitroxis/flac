using System;

namespace Flac.Predictors
{
	internal sealed class Fixed4Predictor : LpcPredictor
	{
		#region Fields

		private int sample1;
		private int sample2;
		private int sample3;

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] coefficients)
		{
			throw new InvalidOperationException("Coefficients of fixed predictors can't be changed.");
		}

		public override void SetSamples(params int[] samples)
		{
			this.sample3 = samples[0];
			this.sample2 = samples[1];
			this.sample1 = samples[2];
		}

		public override int Next(int sample0)
		{
			int value = sample0 * 4 - this.sample1 * 6 + this.sample2 * 4 - this.sample3;
			this.sample3 = this.sample2;
			this.sample2 = this.sample1;
			this.sample1 = sample0;
			return value;
		}

		#endregion
	}
}
