namespace Flac.Predictors
{
	internal sealed class Lpc3Predictor : LpcPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int sample1;
		private int sample2;

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
		}

		public override void SetSamples(params int[] samples)
		{
			this.sample2 = samples[0];
			this.sample1 = samples[1];
		}

		public override int Next(int sample0)
		{
			int value = sample0 * this.coefficient0 + this.sample1 * this.coefficient1 + this.sample2 * this.coefficient2;
			this.sample2 = this.sample1;
			this.sample1 = sample0;
			return value;
		}

		#endregion
	}
}
