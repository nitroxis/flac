﻿using System;

namespace Flac.Predictors
{
	/// <summary>
	/// LPC n-order predictor.
	/// </summary>
	internal sealed class LpcNPredictor : LpcPredictor
	{
		#region Fields

		private readonly int shift;
		private readonly int[] coefficients;
		private readonly int[] samples;
		private readonly int order;
		private int sampleIndex;

		#endregion

		#region Constructors

		public LpcNPredictor(int shift, int[] coefficients, int[] samples)
		{
			this.shift = shift;
			this.order = coefficients.Length;

			this.coefficients = new int[this.order];
			this.samples = new int[this.order - 1];

			this.SetCoefficients(coefficients);
			this.SetSamples(samples);
		}

		#endregion

		#region Methods

		public override void SetCoefficients(params int[] newCoefficients)
		{
			Array.Copy(newCoefficients, this.coefficients, this.order);
		}

		public override void SetSamples(params int[] newSamples)
		{
			Array.Copy(newSamples, this.samples, this.samples.Length);
			this.sampleIndex = 0;
		}

		public override int Next(int x)
		{
			int sum = x * coefficients[0];

			for (int i = 1; i < this.order; i++)
			{
				if (--this.sampleIndex < 0)
					this.sampleIndex = this.order - 2;

				sum += this.samples[this.sampleIndex] * coefficients[i];
			}

			this.samples[this.sampleIndex] = x;
			if (++this.sampleIndex >= this.order - 1)
				this.sampleIndex = 0;

			if (this.shift > 0)
				return sum >> this.shift;

			if (this.shift < 0)
				return sum << (-this.shift);

			return sum;
		}
		
		#endregion
	}
}
