﻿namespace Flac.Predictors
{
	/// <summary>
	/// Represents a predictor that shifts it's values.
	/// </summary>
	internal abstract class LpcShiftPredictor : LpcPredictor
	{
		#region Fields

		public int Shift;

		#endregion

		#region Properties

		#endregion

		#region Methods

		#endregion
	}
}
