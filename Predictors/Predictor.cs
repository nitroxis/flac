﻿using System;

namespace Flac.Predictors
{
	/// <summary>
	/// Represents an abstract predictor.
	/// </summary>
	internal abstract class Predictor
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		#endregion

		#region Methods

		/// <summary>
		/// Predicts the next sample.
		/// </summary>
		/// <param name="sample0">The last sample.</param>
		/// <returns></returns>
		public abstract int Next(int sample0);

		#endregion
	}
}
