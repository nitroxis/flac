﻿using System;

namespace Flac
{
	/// <summary>
	/// Helper for metadata block types.
	/// </summary>
	internal static class MetadataBlockType
	{
		#region Methods

		/// <summary>
		/// Gets the index of the specified block type.
		/// </summary>
		/// <param name="block"></param>
		/// <returns></returns>
		public static int GetType(MetadataBlock block)
		{
			if (block is StreamInfo) return 0;
			if (block is Padding) return 1;
			if (block is Application) return 2;
			if (block is SeekTable) return 3;
			if (block is Comment) return 4;
			if (block is UnknownMetadataBlock) return ((UnknownMetadataBlock)block).Type;

			throw new ArgumentException("Invalid block type.");
		}

		/// <summary>
		/// Gets the type with the specified index.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		public static MetadataBlock CreateMetadataBlock(int type, byte[] data)
		{
			switch (type)
			{
				case 0: 
					return new StreamInfo(data);

				case 1:
					return new Padding(data.Length);

				case 2:
					return new Application(data);

				case 3:
					return new SeekTable(data);

				case 4:
					return new Comment(data);
				
				default:
					return new UnknownMetadataBlock(type, data);
			}
		}

		#endregion
	}
}
