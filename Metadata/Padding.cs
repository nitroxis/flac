﻿namespace Flac
{
	/// <summary>
	/// Represents a padding metadata block.
	/// </summary>
	public sealed class Padding : MetadataBlock
	{
		#region Fields

		private int length;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the length of the padding.
		/// </summary>
		public int Length
		{
			get { return this.length; }
			set { this.length = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new padding metadata block.
		/// </summary>
		public Padding(int length)
		{
			this.length = length;
		}

		#endregion

		#region Methods

		public override byte[] GetData()
		{
			return new byte[this.length];
		}

		#endregion
	}
}
