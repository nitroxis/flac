﻿using System.Runtime.InteropServices;

namespace Flac
{
	/// <summary>
	/// Represents a seek point.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public struct SeekPoint
	{
		#region Fields

		public long Sample;
		public long Offset;
		public ushort SampleCount;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SeekPoint.
		/// </summary>
		public SeekPoint(long sample, long offset, ushort sampleCount)
		{
			this.Sample = sample;
			this.Offset = offset;
			this.SampleCount = sampleCount;
		}

		#endregion

		#region Methods

		#endregion
	}
}
