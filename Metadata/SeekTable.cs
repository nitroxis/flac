﻿using System.Runtime.InteropServices;

namespace Flac
{
	/// <summary>
	/// Represents a seek table metadata block.
	/// </summary>
	public sealed class SeekTable : MetadataBlock
	{
		#region Fields

		private SeekPoint[] table;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the array of seek points.
		/// </summary>
		public SeekPoint[] Table
		{
			get { return this.table; }
			set { this.table = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SeekTable.
		/// </summary>
		public SeekTable(byte[] data)
		{
			int numPoints = data.Length / 18;
			this.table = new SeekPoint[numPoints];

			GCHandle handle = GCHandle.Alloc(this.table, GCHandleType.Pinned);
			Marshal.Copy(data, 0, handle.AddrOfPinnedObject(), data.Length);
			handle.Free();
		}

		#endregion

		#region Methods

		public override byte[] GetData()
		{
			byte[] buffer = new byte[this.table.Length * 18];

			GCHandle handle = GCHandle.Alloc(this.table, GCHandleType.Pinned);
			Marshal.Copy(handle.AddrOfPinnedObject(), buffer, 0, buffer.Length);
			handle.Free();

			return buffer;
		}

		#endregion
	}
}
