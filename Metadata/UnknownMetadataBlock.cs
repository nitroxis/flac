﻿namespace Flac
{
	/// <summary>
	/// Represents a metadata block that is not known to this library.
	/// </summary>
	public sealed class UnknownMetadataBlock : MetadataBlock
	{
		#region Fields

		private readonly int type;
		private readonly byte[] data;

		#endregion

		#region Properties

		public int Type
		{
			get { return this.type; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new UnknownMetadataBlock.
		/// </summary>
		public UnknownMetadataBlock(int type, byte[] data)
		{
			this.type = type;
			this.data = data;
		}

		#endregion

		#region Methods
		
		public override byte[] GetData()
		{
			return this.data;
		}

		#endregion
	}
}
