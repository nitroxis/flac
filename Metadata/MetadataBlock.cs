﻿namespace Flac
{
	/// <summary>
	/// Represents a FLAC metadata block.
	/// </summary>
	public abstract class MetadataBlock
	{
		#region Methods

		/// <summary>
		/// Gets the data as byte array.
		/// </summary>
		/// <returns></returns>
		public abstract byte[] GetData();

		#endregion
	}
}
