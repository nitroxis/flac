﻿using System.IO;

namespace Flac
{
	/// <summary>
	/// Stream helper methods.
	/// </summary>
	internal static class StreamExtensions
	{
		#region Methods

		/// <summary>
		/// Reads the specified number of bytes and blocks the executing thread until all bytes have been read or throws an EndOfStreamException if the stream ended.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		public static void ReadBlocking(this Stream stream, byte[] buffer, int offset, int count)
		{
			while (count > 0)
			{
				int numBytes = stream.Read(buffer, offset, count);
				if (numBytes == 0)
					throw new EndOfStreamException();

				count -= numBytes;
				offset += numBytes;
			}
		}

		/// <summary>
		/// Reads the specified number of bytes and blocks the executing thread until all bytes have been read or throws an EndOfStreamException if the stream ended.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static byte[] ReadBlocking(this Stream stream, int length)
		{
			byte[] buffer = new byte[length];
			stream.ReadBlocking(buffer, 0, length);
			return buffer;
		}

		#endregion
	}
}
