﻿namespace Flac
{
	/// <summary>
	/// Inter-channel decorrelation functions.
	/// </summary>
	public static class InterChannel
	{
		#region Methods

		/// <summary>
		/// Decodes stereo data in the left / side (difference) format.
		/// </summary>
		/// <param name="left">The left channel. Unchanged.</param>
		/// <param name="right">Input: side/difference data; Output: right channel.</param>
		/// <param name="length">The length of the data.</param>
		public static void LeftSide(int[] left, int[] right, int length)
		{
			for (int i = 0; i < length; i++)
				right[i] = left[i] - right[i];
		}

		/// <summary>
		/// Decodes stereo data in the right / side (difference) format.
		/// </summary>
		/// <param name="left">Input: side/difference data; Output: right channel.</param>
		/// <param name="right">The right channel. Unchanged.</param>
		/// <param name="length">The length of the data.</param>
		public static void RightSide(int[] left, int[] right, int length)
		{
			for (int i = 0; i < length; i++)
				left[i] += right[i];
		}

		/// <summary>
		/// Decodes stereo data in the mid (average) / side (difference) format.
		/// </summary>
		/// <param name="left">Input: mid/average data; Output: left channel.</param>
		/// <param name="right">Input: side/difference data; Output: right channel.</param>
		/// <param name="length">The length of the data.</param>
		public static void MidSide(int[] left, int[] right, int length)
		{
			for (int i = 0; i < length; i++)
			{
				int side = right[i];
				int mid = (left[i] << 1) | (side & 1);
				left[i] = (mid + side) >> 1;
				right[i] = (mid - side) >> 1;
			}
		}

		#endregion
	}
}
